class Conectores:
    saldo = 0.0
    numero_cuenta = 0

    def __init__(self, saldo, numero_cuenta):
        self.saldo = saldo
        self.numero_cuenta = numero_cuenta

    def existe_saldo(self):
        return self.saldo > 0

    def retirar(self, monto):
        if monto < self.saldo:
            self.saldo = self.saldo - monto
        else:
            print("No existe el suficiente saldo!!!")

    def depositar(self, monto):
        self.saldo = self.saldo + monto

    def __str__(self):
        return f"cta{self.numero_cuenta}.${self.saldo:.2f}"

    def __repr__(self):
        return self.__str__()


class Cliente:
    cedula = " "
    nombres = ""
    apellidos = ""
    cuentas_bancarias = []

    def __init__(self,cedula, nombres, apellidos):
        self.cedula = cedula
        self.nombres = nombres
        self.apellidos = apellidos
        self.cuentas_bancarias = []

    def sumar_saldos(self):
        # código temporal
        return self.cuentas_bancarias[0].saldo + self.cuentas_bancarias[1].saldo

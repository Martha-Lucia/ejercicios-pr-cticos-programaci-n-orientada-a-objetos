class Clasificacion:
    nombre = ""
    ejemplo = ""

    def __init__(self, nombre,ejemplo):
        self.tipo = nombre
        self.ejemplo = ejemplo
        print(nombre, 'Es un tipo de conector.')
        print(ejemplo, 'Si tiene.')


class Disyuntivo(Clasificacion):

    def __init__(self):
        print('El conector disyuntivo es el que establece una opción.')
        Clasificacion.__init__(self,'Disyuntivo:', "Ejemplo:")


nombre1 = Disyuntivo()
nombre2 = Clasificacion("Causales: ", "¿Tiene actividades?,")
nombre3 = Clasificacion("Demostrativos: ", "¿Tiene ejemplos?",)



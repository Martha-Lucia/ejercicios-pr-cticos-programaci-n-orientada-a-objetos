class Conectores:
    tipo = ""
    definicion = ""
    lista = []

    def __init__(self, tipo):
        self.definicion = ""
        self.tipo = tipo
        self.lista = []
        print(self.tipo)


a = Conectores("Disyuntivas:")
a.definicion = "Establecen una opción."
print(a.definicion)
a.lista = ["o", "bien"]
print(a.lista)


b = Conectores("Causales:")
b.definicion = "Introducen una causa."
print(b.definicion)
b.lista = ["porque", "ya que", "dado que"]
print(b.lista)


c = Conectores("Temporales:")
c.definicion = "Sitúan el texto en un momento."
print(c.definicion)
c.lista = ["anteriormente", "al principio", "previamente", "al mismo tiempo", "finalmente" ]
print(c.lista)


d = Conectores("Explicativos:")
d.definicion = "Explican alguna idea compleja."
print(d.definicion)
d.lista = ["por ejemplo", "es decir", "en otras palabras", "quiero decir", "o sea"]
print(d.lista)


class Actividades:
    seleccion = ""
    opcion_multiple = ""

    def __init__(self, selec):
        self.seleccion = selec
        print("Actividades de selección: ")

    def Puntos(self, sum):
        self.opcion_multiple = sum
        print(self.opcion_multiple)


ex = Actividades("Selec")
ex.Puntos("Este es el puntaje de la actividad: ")